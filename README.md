# SFI FreeLdr

SFI systems support for the ReactOS FreeLoader

## Building

Copy files from "new" and "upd" directories to the ReactOS sources and build ReactOS.
Files from "base" directories is unmodified copy of changed files, useful to trace changes.

## Running

Run freeldr.sys as multiboot module, use "bootpath" command line argument to specify path to freeldr.ini. VGA BIOS can be passed as additional multiboot module with "VgaBios" string as its command line.

Example:
rawexec mboot freeldr.sys "bootpath=sdhci(8)sd(0)partition(1)" VgaBiosGMA600.bin VgaBios

bootpath can be sdhci(x)emmc(x)mmcp(x)partition(x) or sdhci(x)sd(x)partition(x).
If no bootpath is defined, a list of detected devices will be displayed.
